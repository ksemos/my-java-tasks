/*10.55 �������� ����������� ��������� �������� ������������ ����� �� �������-
��� ������� ��������� � N-������. �������� N � �������� ��������� ���-
����� � ���������� (2 N 16).
*/
import java.util.Scanner;

public class TaskCh10N055{
    public static void main(String[] args) {
     
    int N;//������� ���������
	int n;//�����
	
	
     Scanner sc = new Scanner(System.in); 
	 //������ �����
	 System.out.println("Enter positive integer\n");
	   if(sc.hasNextInt()) 
          n = sc.nextInt(); 
       else {
          System.out.println("Incorrect input"); return;
	    }
     //������ ������� ���������
	 System.out.println("Enter numeral system\n");
	   if(sc.hasNextInt()) 
          N = sc.nextInt(); 
       else {
          System.out.println("Incorrect input"); return;
	    }
  
	 System.out.printf("%d in %d-system: \n",n,N);
	 Convert(n,N);
}
    // ����������� ��������� ��� �������� ������������ ����� n �� ��������� ������� � N-������ (N �� 2 �� 16)
   
   static void Convert(int n,int N) {
        char []C ={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
		if(N<2 || N>16)	{
           System.out.println("Incorrect value of N");
		   return;
		}
		
		if (n/N != 0) 
		  Convert(n/N,N);
		
        System.out.printf("%c",C[n%N]);
        return;
    }

}
