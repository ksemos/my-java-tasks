/*6.87. ��������� ���������, ������� ����� ���� �����, ��������� ������ �������� ��� ���� � ���������.
 ���������� �����, ���������� ��������� � ���� ����, ����� ���� ����� 1, 2 ��� 3. 
 ����� ������ ��������� ���� �������� �� �����. ����� ��������� ���� ������ �������� ��������� � ������� ����� �������-��������������.
 ��������� ���� ������� ������������ ������ ���������� �����, ������� ����.*/
import java.util.Scanner;

public class TaskCh06N087
{
    public static void main(String[] args) {
		
	Game g =new Game();	
	g.play();
       
	}
}



	 class Game{
	   
	    String TeamName1,TeamName2;
		int TeamScore1=0,TeamScore2=0;
	   /** here will be implemented console input logic for 2 players*/
	   void play()
	   {
	
		int team_num=-1,curr_score;

		
		Scanner sc = new Scanner(System.in); 
		System.out.println("Enter team #1");
	    TeamName1 = sc.next(); 
		System.out.println("Enter team #2");
	    TeamName2 = sc.next(); 
		
		while (team_num!=0) 
		{
			System.out.println("Enter team to score (1 or 2 or 0 to finish game)");
			if(!sc.hasNextInt()) {sc.next(); team_num=-1;}
			else
				team_num = sc.nextInt(); 
			
			if (team_num == 0) break;
						
			System.out.println("Enter score (1 or 2 or 3)");  
			if(!sc.hasNextInt()) {sc.next();curr_score=-1;}
			else curr_score = sc.nextInt(); 
			if(curr_score!=1 && curr_score!=2 && curr_score!=3) System.out.println("Incorrect score"); 
			else
			{
	          switch (team_num) {
			  case 1: TeamScore1+=curr_score; break;
			  case 2: TeamScore2+=curr_score; break;
			  default: System.out.println("Incorrect team number"); break;}
		    }
			  
	   	   score();
		}
		result();
	  
	}
	
	/** intermediate score*/
    String score()
	{
		String s;
		s= "Intermediate result: "+TeamName1+" vs "+TeamName2+"    "+TeamScore1+" - "+TeamScore2;
		System.out.println(s);
		return s;
	}
	
	/** result with winner, loser and their scores*/
	String result()
	{
		String s ="";
		if (TeamScore1>TeamScore2)   
       		s= "The winner is "+TeamName1+". The looser is "+TeamName2+". Total result: "+TeamName1+" vs "+TeamName2+"    "+TeamScore1+" - "+TeamScore2;
		if (TeamScore1<TeamScore2)   
       		s= "The winner is "+TeamName2+". The looser is "+TeamName1+". Total result: "+TeamName1+" vs "+TeamName2+"    "+TeamScore1+" - "+TeamScore2;
	    if (TeamScore1==TeamScore2)   
       		s="The game ended in a draw. Total result: "+TeamName1+" vs "+TeamName2+"    "+TeamScore1+" - "+TeamScore2;
		System.out.println(s);
		return s;
	}
	
	
	
}
	 		