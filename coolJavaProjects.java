import java.util.*;
  public class coolJavaProjects{
    public static void main(String[] args) {
		
        ArrayList< Candidate > list = create_list();//создадим тестовые данные
		//выводим диалог
		Candidate.Dialog(list);
	}   		
		//создадим тестовые данные с кандидатами двух типов
		public static ArrayList< Candidate > create_list()
		{
		  JavaJob jj;
		  SelfEdu se;
		  ArrayList< Candidate > list = new ArrayList< Candidate >(10);
		  for(int i=0;i<10;i++)
		  {
	
		     if (i%2==0){
				 jj=new JavaJob("Ivan");
			     list.add(jj);
			 }
			 else {
				 se=new SelfEdu("Petr");
			     list.add(se);
			 }
		  }
		  return list;
	 	}
  }
	
	
	
	// создадим абстрактный класс кандидатов, состоящий из двух подклассов
	abstract class Candidate
	{
	  public String name;//здесь будет храниться имя кандидата
	 
	 //public String type;//где учился
	  //общий метод, представление кандидата 
	  public void say_name ()
	  {
		 System.out.println("hi! my name is "+name);
	  }
	  
	  //абстрактный метод вывода информации об обучении, конкретная реализация в наследственных классах
	  public abstract void say_exp();//используем полиморфизм, определим как конкретно метод ведет себя с кандидатом в зависимости от типа кандидата

	//Выводит диалог между работодателем и кондидатами
	//list -заполненный список кандидатов
	 public static void Dialog(ArrayList< Candidate > list)
	 {
	   for (int i=0;i<list.size();i++)
		{
		    System.out.println("\n "+i+") hi! introduce yourself and describe your java experience please.");
			list.get(i).say_name();
		    list.get(i).say_exp();
			
		}
	   }
	 }
	
	//наследование классов, класс кандидатов закончивших курсы getjavajob
	  class JavaJob extends Candidate{
		  //
		  public void say_exp ()
		  { 
		    System.out.println("I've passed successfully getJavaJob exams and code rewiws.");
		  }
	
	public JavaJob (String t_name)
		  {
			  name=t_name;
		  }
		  
	  }
	  //наследование классов, класс самостоятельно учивших кандидатов
	 class SelfEdu extends Candidate{
		  public void say_exp ()
		  { 
		    System.out.println("I've been learning Java by myself, nobody examined how thorough is my knowledge and how good is my code.");
		  }
		  
		  public SelfEdu (String t_name)
		  {
			  name=t_name;
		  }
	  }
	


