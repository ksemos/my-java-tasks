/*11.158.*������� �� ������� ��� ������������� ��������, ������� �� ������ ���-������,
 �. �. � ������� ������ �������� ������ ��������� ��������.
*/
import java.util.*;

class TaskCh11N158{
    public static void main(String[] args) {

       int [] OldArray = {0,1,2,3,4,5,3,7,8,3,3};//�������� ������]
	   int [] NewArray = new int[OldArray.length];//��������������� ������
	   int zero_pos=-1;//������� ���� � �������
	   int k=0;
	   System.out.println("Origin array: "+Arrays.toString(OldArray));
	   
	   //���������, �������� �� ������ ���� � ���������� �������, ���� ��������
	    for (int i=0;i<OldArray.length;i++)
			if (OldArray[i]==0)
		       zero_pos=i;
	   //��������� ����������
	 	for (int i=0;i<OldArray.length;i++)
		{
			for(int j=i+1;j<OldArray.length;j++)//���� ������� ����������� �������� ����������� � ��������� ��� ������� i, �������� ��
				if(OldArray[j]==OldArray[i])
					OldArray[j]=0;
			
			if (OldArray[i]!=0 || i==zero_pos)
			{NewArray[k]=OldArray[i];
			k++;
			}
		}
				//��������� ������
		for(int i=k;i<NewArray.length;i++)
			NewArray[i]=0;
		//������� ����� ������ �� �����
		System.out.println("New array: "+Arrays.toString(NewArray));
    }
	
}