/*10.43. �������� ����������� �������:
�) ���������� ����� ���� ������������ �����;
�) ���������� ���������� ���� ������������ �����.*/
public class TaskCh10N043{
    public static void main(String[] args) {
     
   	int n=1060;//	 �������� �����
	System.out.printf("The number is: %d. The sum of digits is %d\n",n,summ(n));//a)
	System.out.printf("The number is: %d. Quantity of digits is %d",n,digit_num(n));//b
}
    // ����������� �������, ���������� ����� ���� n
   static int summ(int n) {
       
	   int res;

        if (n == 0)
            return 0;
        res = summ(n/10) + n%10;
        return res;
    }
	// ����������� �������, ���������� ���������� ���� n
   static int digit_num(int n) {
       
	   int res;

        if (n == 0)
            return 0;
        res = digit_num(n/10) + 1;
        return res;
    }
}
