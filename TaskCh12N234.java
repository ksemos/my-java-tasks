/*12.234. ��� ��������� ������.
�) ������� �� ���� k-� ������.
�) ������� �� ���� s-� �������.* 
*/
import java.util.*;

public class TaskCh12N234{
    public static void main(String[] args) {
    
	int k_col=2;//����� ���������� �������
	int k_row=3;//����� ��������� ������
	int[][] Arr = new int[11][10];//�������� ������
    //�������� ������ ��������� �������	
	for (int i=0;i<11;i++)
		 for(int j=0;j<10;j++)
			 Arr[i][j]=i+j;
	//������ ��������� �������	
	System.out.println("Origin array") ;
    print_array(Arr);
    //������ ����� �������� ������
	System.out.printf("After deleting %d row \n",k_row) ;
	print_array(row_del(Arr,k_row));
    //������ ����� �������� �������
	System.out.printf("After deleting %d column \n",k_col) ;
	print_array(col_del(Arr,k_col));
}

//�) ������� �� ���� k-� ������.
   static int[][]  row_del(int[][] arr,int k) 
   {
    int i=k;
	int zero[] = new int[arr[0].length];
	if(k>=arr.length) return arr;//���� ����� ������ ������ ������� �������, �� ������ �� ������
	//�������� ������
	for(i=k;i<arr.length-1;i++)
		 arr[i]=arr[i+1];
	//��������� ��������� ������ ������
	Arrays.fill(zero,0);
    arr[i]=zero;
	 	  
	 return arr;
    }
	
	//�) ������� �� ���� s-� �������.
   static int[][]  col_del(int[][] arr,int s) 
   {
	if(s>=arr[0].length) return arr;//���� ����� ������ ������ ������� �������, �� ������ �� ������
	//�������� �������
	for(int i=0;i<arr.length;i++)
		for(int j=s;j<arr[0].length;j++)
		{ 
	     if (j!=arr[0].length-1)
		   arr[i][j]=arr[i][j+1];
	     else
			  arr[i][j]=0;//��������� ��������� ������
		}
	
	 	  
	 return arr;
 
   }
   
//������ ������� ����������    
	static void print_array(int[][] arr)  
	 {
        int n = arr.length;//���������� ����� � �������		
		for(int i=0;i<n;i++)
			System.out.println(Arrays.toString(arr[i])) ;
		System.out.println("") ;
	 }
}