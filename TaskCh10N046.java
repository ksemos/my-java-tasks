/*10.46. ���� ������ ���� � ����������� �������������� ����������. �������� ��-
��������� �������:
�) ���������� n-�� ����� ����������;
�) ���������� ����� n ������ ������ ����������*/
public class TaskCh10N046{
    public static void main(String[] args) {
     
   	double a=4;// ������ ���� �������������� �����������
	double d=2;// �������� ����������� ����������
	int n=3;//
	System.out.printf("The first member is %f, the difference is %f\n, %d-member is %f, summ of %d members is %f\n",a,d,n,memb(a,d,n),n,summ(a,d,n));
}
    // ����������� �������, ���������� ������� n ����� ����������
   static double memb(double a,double d,int n) {
       
	   double res;

        if (n == 1)
            return a;
        res = memb(a,d,n-1)*d;
        return res;
    }
	// ����������� �������, ���������� ������� ����� n ������ ������ ����������.
   static double summ(double a,double d,int n) {
       
	   double res;

        if (n == 1)
            return a;
        res = summ(a,d,n-1)+memb(a,d,n);
		return res;
    }
}
