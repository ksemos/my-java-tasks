/*10.51.* 
*/
public class TaskCh10N051{
    public static void main(String[] args) {
     
    int n=5;
	System.out.printf("a)");
	proc1(n);
	System.out.printf("\n b)");
	proc2(n);
	System.out.printf("\n c)");
	proc3(n);
}

   static void  proc1(int n) {
       
	    if (n > 0) {
			System.out.printf("%d",n);
			proc1(n-1);
		   }

        return;
    }

	   static void  proc2(int n) {
       
	    if (n > 0) {
				proc2(n-1);
				System.out.printf("%d",n);
		   }

        return;
    }
	
	  static  void  proc3(int n) {
       
	   if (n > 0) {
		       System.out.printf("%d",n);
				proc3(n-1);
				System.out.printf("%d",n);
		   }

        return;
	   }
}