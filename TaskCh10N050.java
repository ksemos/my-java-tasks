/*10.50. �������� ����������� ������� ��� ���������� �������� ��� ����������
������� ��������� ��� ��������������� ����� n � m.
������� ��������� �������� ������ �����������, �. �. ���� ������� �
���� �� �� ���������� ���������� ����� ����� ����.
*/
public class TaskCh10N050{
    public static void main(String[] args) {
     
    int n=1,m=3;//�������� ��������
	System.out.printf("Akkerman function A(%d,%d) = %d",n,m,Akkerman(n,m));
}
    // ����������� �������, ���������� ����� ������������� �������� �������
   static int Akkerman(int n,int m) {
       
	   int res=0;
		if ( n<0 || m<0 )
			res = -1;
        if (n == 0)
            res = m+1;
		if (m == 0 && n!=0)
            res = Akkerman(n-1,1);
		if(n>0 && m>0)
		   res = Akkerman(n-1,Akkerman(n,m-1));
        
        return res;
    }

}
