/*13.12. �������� ���������� � 20 ����������� �����: �������, ���, ��������, ����� � ���� ����������� �� ������ (�����, ���).
 ���������� �������, ���, �������� � ����� �����������, ������� �� ����������� ���� �������-���� � ����� �� ����� ���� ���.
 ���� ������ �� ��������� (��� ���������� ������ ����������� � ������ ������������ ��� �������, ��� ������ ���-��� ���).
*/
import java.util.*;

public class TaskCh13N012{
    public static void main(String[] args) {
    
		int n=20;//���������� �����������
		ArrayList <Employee> emplist=Employee.TestData(n);//��������� ������ � �����������
		Employee.Print_list(emplist);//������ ���������� ��� ���� ���������� �� ������
		Employee.Print_spec(emplist);
		//�������� ���������� � ����������� ������� ����������� ������ ���� ���
	}
	

}

 class Employee{

	 public String Surname;
	 public String Name;
	 public String MidName;
	 public String Adress;
	 public int Month;
	public int Year;
	
//��������� ������������ ������
	public Employee() 
	  {
        Surname = "";
        Name="";
        MidName="";
		Adress="";
		 Month=0;
		 Year=0;
    }
	public Employee(String surname,String name, String midname,
	  String adress, int month, int year) 
	  {
        Surname = surname;
        Name=name;
        MidName=midname;
		Adress=adress;
		 Month=month;
		 Year=year;
    }
	////��������� �������� ������ � �����������. �������� ���������� n ����������� � ���������� ������ �� ������
	public static ArrayList TestData(int n)
	{
	 
       ArrayList <Employee> emplist= new ArrayList <Employee> (n);//������ �����������
	   Employee emp;
	   for (int i=0;i<n;i++)
	   {
		 emp = new  Employee("Petrov","Ivan",(i%12+1)+" "+(2010+i%7),"Moscow, Vavilova 1-1",i%12+1,2010+i%7);
		 emplist.add(emp);
	    }
		return emplist;
	}
    //������ ���������� �� ����� ����������
	//emp - ���������
	public  static void Print_info(Employee emp)
	{
	   System.out.println("Surname: "+emp.Surname+", name: "+emp.Name+", middle name: "+emp.MidName+", address: "+emp.Adress);
		
	}
	
	//������ ���������� ��� ���� ���������� �� ������
	//emplist - ������ ���� �����������
	public  static void Print_list(ArrayList <Employee> emplist)
	{
         System.out.println("Information about all workers: surname, name, middle name, address, month of employment, year of employment.");
		 for(int i=0;i<emplist.size();i++)
			System.out.println(emplist.get(i).Surname+" "+emplist.get(i).Name+" "+emplist.get(i).MidName+" "+emplist.get(i).Adress+" "+emplist.get(i).Month+" "+emplist.get(i).Year);
		 System.out.printf("\n \n");
		
	}
	
		
		//�������� ���������� � ����������� ������� ����������� ������ ���� ���
		//emplist - ������ ���� �����������
	    public  static void Print_spec(ArrayList <Employee> emplist)
	   {
		 Calendar calendar = new GregorianCalendar();
		 Date myDay = calendar.getTime();//������� ����
		 int Month=myDay.getMonth()+1;//�������� ������� �����
		 int Year=myDay.getYear ()+1900;//�������� ������� ���
		 
		 System.out.println("Information about employees, who've been working for more than 3 years \n");
		 for (int i=0;i<emplist.size();i++)
			if (Year-emplist.get(i).Year+Math.min(0,Math.signum(Month-emplist.get(i).Month))>=3)
				Employee.Print_info(emplist.get(i));
	   }
 }
